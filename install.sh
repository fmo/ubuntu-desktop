#!/bin/bash
echo "Updating system and installing all pre-requisites"
sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get install dkms open-vm-tools-desktop python-dev ruby-dev git build-essential python-setuptools libssl-dev && sudo easy_install pip && sudo pip install --upgrade ansible

echo "Cloning the ansible code from github"
git clone https://github.com/fmoreira/ubuntu-desktop.git
cd ubuntu-desktop

echo "Running playbook"
ansible-playbook -i hosts site.yml -c local -K

echo "Cleaning up"
rm -rf ubuntu-desktop

echo "Installing oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

echo "Installing LinuxBrew"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"
echo 'export PATH="$HOME/.linuxbrew/bin:$PATH"' >>~/.zshrc
